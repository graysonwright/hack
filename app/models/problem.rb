class Problem < ActiveRecord::Base
  attr_accessible :answer, :content, :title

  belongs_to :author, class_name: "User", foreign_key: "author_id"

  validates :title, presence: true, uniqueness: true
  validates :content, presence: true
  validates :answer, presence: true
end

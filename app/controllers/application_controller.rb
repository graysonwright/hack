class ApplicationController < ActionController::Base
  protect_from_forgery

  # Tells the CanCan gem to ensure that authorization
  # happens on every action in the application
  check_authorization unless: :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: exception.message
  end

end

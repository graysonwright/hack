require 'spec_helper'
require 'cancan/matchers'

describe User do
  describe "attribute 'admin'" do
    it 'defaults to false' do
      user = Factory :user
      user.admin.should be false
    end

    # Mostly to test that the factory is working
    it 'is true for admin user' do
      user = Factory :admin
      user.admin.should be true
    end
  end

  describe "abilities" do
    subject { Ability.new(Factory user_type) }

    context "when is an admin" do
      let(:user_type) { :admin }

      it('can manage new problems') { should be_able_to :manage, Problem.new }
    end

    context "when is a regular user" do
      let(:user_type) { :user }

      it("can't manage problems") { should_not be_able_to :manage, Problem.new }
      it("can create problems") { should be_able_to :create, Problem.new }
    end
  end
end

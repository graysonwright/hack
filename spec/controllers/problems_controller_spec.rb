require 'spec_helper'

describe ProblemsController do
  before(:each) {sign_in user}

  context 'when user is signed in' do

    let(:user) {Factory :user}

  end

  context "when admin is signed in" do
    let(:user) {Factory :admin}

    describe "GET #index" do
      it 'populates an array of problems' do
        problem = Factory :problem
        get :index
        assigns(:problems).should eq([problem])
      end

      it 'renders the :index template' do
        get :index
        response.should render_template :index
      end
    end

    describe "GET #show" do
      it 'assigns the requested problem to @problem' do
        problem = Factory :problem
        get :show, id: problem
        assigns(:problem).should eq(problem)
      end

      it 'renders the :show template' do
        get :show, id: Factory(:problem)
        response.should render_template :show
      end
    end

    describe "GET #new" do
      it 'renders the :new template' do
        get :new
        response.should render_template :new
      end
    end

    describe "POST #create" do
      context "with valid attributes" do
        it "creates a new problem" do
          expect {
            post :create, problem: Factory.attributes_for(:problem)
          }.to change(Problem, :count).by(1)
        end

        it "redirects to the new problem" do
          post :create, problem: Factory.attributes_for(:problem)
          response.should redirect_to Problem.last
        end

        it 'associates new problem with current user' do
          post :create, problem: Factory.attributes_for(:problem)
          Problem.last.author.should eq(user)
        end
      end

      context "with invalid attributes" do
        it "does not save the new problem" do
          expect {
            post :create, problem: Factory.attributes_for(:invalid_problem)
          }.to_not change(Problem, :count)
        end

        it "re-renders the new template" do
          post :create, problem: Factory.attributes_for(:invalid_problem)
          response.should render_template :new
        end
      end
    end
  end

end

require 'spec_helper'

describe 'Welcome page' do
  context "when not signed in" do
    it "should link to sign up page" do
      visit root_path
      click_link 'Click here to sign up'
      current_path.should eq new_user_registration_path
    end
  end

  context "when signed in" do
    before(:each) { login_as Factory(:user), scope: :user }

    it "should link to new problem page" do
      visit root_path
      click_link 'Create a problem'
      current_path.should eq new_problem_path
    end

  end
end

require 'spec_helper'

describe "UserSessions" do
  describe "Sign in page" do
    it "displays correctly" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      get new_user_session_path
      response.status.should be(200)

      visit new_user_session_path
      page.should have_content 'Sign in'
      page.should have_content 'Email'
      page.should have_content 'Password'
      page.should have_content 'Remember me'
    end

    it 'lets valid user sign in' do
      user = Factory :user
      visit new_user_session_path
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Sign in'
      page.should have_content 'Signed in successfully'
    end

    it "doesn't let invalid user sign in" do
      visit new_user_session_path
      fill_in 'Email', with: "nobody@example.com"
      fill_in 'Password', with: "secret"
      click_button 'Sign in'
      page.should have_content 'Invalid email or password'
    end
  end

  describe "Registrations page" do
    it 'displays correctly' do
      get new_user_registration_path
      response.status.should be(200)

      visit new_user_registration_path
      page.should have_content 'Sign up'
      page.should have_content 'First name'
      page.should have_content 'Last name'
      page.should have_content 'Email'
      page.should have_content 'Password'
      page.should have_content 'Password confirmation'
    end

    it 'validates email has correct format' do
      visit new_user_registration_path
      fill_in 'Email', with: 'invalid_email'
      fill_in 'user_password', with: 'secret'
      fill_in 'user_password_confirmation', with: 'secret'
      click_button 'Sign up'

      page.should have_content 'Please review the problems below'
      page.should have_content 'is invalid'
    end

    it 'validates password is long enough' do
      visit new_user_registration_path
      fill_in 'Email', with: 'valid_email@example.com'
      fill_in 'user_password', with: 'abcde'
      fill_in 'user_password_confirmation', with: 'abcde'
      click_button 'Sign up'

      page.should have_content 'Please review the problems below'
      page.should have_content 'is too short'
    end

    it 'validates password matches confirmation' do
      visit new_user_registration_path
      fill_in 'Email', with: 'valid_email@example.com'
      fill_in 'user_password', with: 'secret'
      fill_in 'user_password_confirmation', with: 'password'
      click_button 'Sign up'

      page.should have_content 'Please review the problems below'
      page.should have_content "doesn't match confirmation"
    end

    it 'lets user sign up' do
      visit new_user_registration_path
      fill_in 'Email', with: 'test@example.com'
      fill_in 'user_password', with: 'secret'
      fill_in 'user_password_confirmation', with: 'secret'
      click_button 'Sign up'

      page.should have_content 'signed up successfully'
    end

    it 'validates email is unique' do
      user = Factory :user
      visit new_user_registration_path
      fill_in 'Email', with: user.email
      fill_in 'user_password', with: 'a_different_password'
      fill_in 'user_password_confirmation', with: 'a_different_password'
      click_button 'Sign up'

      page.should have_content 'Please review the problems below'
      page.should have_content "has already been taken"
    end
  end
end

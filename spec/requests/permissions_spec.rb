require 'spec_helper'

describe "Permissions" do
  describe "guest user" do

    it "can't see restricted pages" do
      visit problems_path
      page.should have_content 'not authorized'
    end

  end
end

Factory.define :user do |f|
  f.sequence(:email) { |n| "foo#{n}@example.com" }
  f.password "secret"
end

Factory.define :admin, parent: :user do |f|
  f.admin true
end

Factory.define :problem do |f|
  f.sequence(:title) { |n| "Foo #{n}" }
  f.content "Lorem ipsum dolor si amet"
  f.answer "42"
end

Factory.define :invalid_problem, parent: :problem do |f|
  f.title nil
end

# Hack
By Grayson Wright

- - -

Hack is a website designed for MSU Computer Science competitions.

Students will compete to solve a list of problems, where the answer to
each problem will point them to the next. At the end of the given time,
whichever student has made it the farthest wins.

## Technical Details

Hack is created using the [Ruby on Rails][rails] framework.


**The site is currently under development**, so stay tuned for more info soon!


[rails]: http://www.github.com/rails/rails
